## Project Description 
This repository contains items from the paper "[Construction and Consumption of FAQ Knowledge Graphs in the E-Commerce Domain](https://ieeexplore.ieee.org/abstract/document/10455954/)". 

## Files' and Folders' Description 
- dataset.csv: Contains all entities and metadata used in KG creation and consumption, scraped from [Tokopedia's FAQ page](https://tokopedia.com/help) per February 29th 2023
- knowledgegraph.ttl: The constructed Knowledge Graph 
- SPARQL queries: Folder containing 10 SPARQL queries that can be run using Apache Jena Fuseki. 

## How to open Apache Jena Fuseki to run SPARQL queries 

### Installing and Opening Apache Jena Fuseki


1. Make sure you have the latest version of Java installed for your operating system (MacOS/Windows/Linux) through the link [here](https://www.oracle.com/java/technologies/downloads/).

2. Download the latest version of Apache Jena Fuseki through the link [here](https://jena.apache.org/download/). For this tutorial, we are downloading the zip file of the current newest version (5.1.0).

3. Extract the downloaded zip file. The extracted folder should be named "apache-jena-fuseki-<version>", like "apache-jena-fuseki-5.1.0".

For MacOS:

4. Click on the "fuseki-server" file inside the extracted folder. The "terminal" application should open automatically and you should see something like "`12:18:51 INFO  Server  :: Started 2024/08/07 12:18:51 WIB on port 3030`"

5. Open your web browser and type in the link "`localhost:<port>`" to access the homepage of Apache Jena Fuseki. In this tutorial, because the default port is 3030, the link is `localhost:3030`.

For Windows:

4. Create a folder named "data" inside the extracted folder.

5. Open command prompt, go to the extracted folder's directory using `cd`. Example: `cd .../apache-jena-fuseki-5.1.0`

6. Type and run the command `fuseki-server --loc=data --update /your-dataset`. The "your-dataset" can be replaced with any name that will be your dataset.

7. Open your web browser and type in the link "`localhost:<port>`" to access the homepage of Apache Jena Fuseki. In this tutorial, because the default port is 3030, the link is `localhost:3030`.

### Run Queries 

1. Create a dataset. If this is your first time accessing Apache Jena Fuseki and you see "No datasets created - add one" on the homepage, click on "add one". Another way is by clicking on "manage" at the navigation bar and then click "new dataset". If you are using Windows and have created a dataset, skip to step 3.

2. Fill in the dataset name and choose "persistent" for dataset type. Click "create dataset". You should be redirected to the Manage Datasets page.

3. Click on "add data" on the row of your newly created dataset.

4. Click on "select file". Choose the "knowledgegraph.ttl" file from the repository. Click on "upload now". In the status column, the green bar should reach 100 and you should see "Triples uploaded: 14845" under it.

5.  Click on "query" tab.

6. Copy and paste each query from the "SPARQL queries" folder, and click on the run button on the far right of the text field (the button looks like a sideways triangle).